=========
Overview
=========

HOStools:

An Analysis tool for higher order discretization methods 
used in Geophysical Modeling and Fluid Mechanics studies.
This documentation describes an efficient implementation
and performance tests on Supercomputers.

This documentation is under development.

Installation:
=============

To install, type the following command::

  ~$ pip install hos-tools

Uninstallation:
===============

To uninstall, type the following command::

  ~$ pip uninstall hos-tools
  
Features:
=========

The following analysis can be performed:
   #. Phase and group velocity errors analysis
   #. Linear stability analysis
   #. Aliasing (Non-linear stability) analysis

License:
========

MIT License
