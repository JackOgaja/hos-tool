/*
-----------------------------------------
Copyright (C) 2014-2016, Jack Ogaja
All rights reserved

See the file "LICENSE" for information on
usage and redistribution of this file

@[+]hos.h 0.1.0 
<jack.ogaja+ at +gmail.com> 06-16-14
-----------------------------------------
*/

/*****************

DESCRIPTION:

hos' main
  - data structures;
  - MACROS;
  - functions prototypes;

******************/

#ifndef HOS_H
#define HOS_H

#include <stdio.h>
#include <math.h>
#include "cpp_defs.h"

BEGIN_DECL_CPP

//#define INPUT_PARAMETER = 100 /*temp definition */
#define INPUT_PARAM 0xfffff /* reserve memory for settings */
#define PARAM_VALUE 0xbbbbb /* input parameter value */

#define SQRT_VAR 0.01
#define WAVE_VAR1 2000
#define WAVE_VAR2 6000
#define WAVE_VAR3 4000
#define ANA_VAR 1000

#define delta 0.0003
#define delta_d 0.0000001

typedef long int Lint;
typedef unsigned int Uint;
//typedef long float Lfloat;
typedef long double Ldouble;

#define int_bytes sizeof(int)
#define char_bytes sizeof(char)
#define float_bytes sizeof(float)
#define double_bytes sizeof(double)

#define Lint_bytes sizeof(Lint)
#define Uint_bytes sizeof(Uint)
//#define Lfloat_bytes sizeof(Lfloat)
#define Ldouble_bytes sizeof(Ldouble)

#define deg_to_rad(x) (x*180/M_PI) /* convert degree to rads */

void *wave(int nx, float *u_wave);
float *fourth_order (int nx, float dx, float *u);
float *second_order_qcs (int nx, float dx, float *u_s);
float *fourth_order_qcs (int nx, float dx, float *u_f);
float *sixth_order_qcs (int nx, float dx, float *u_s);

FILE *open_file (char *fileName);
struct read_config *read_conf (FILE *configFile);

END_DECL_CPP

#endif /*-- END HOS_H --*/


