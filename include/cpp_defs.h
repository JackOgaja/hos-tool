/*
-----------------------------------------
Copyright (C) 2014-2018, Jack Ogaja
All rights reserved

See the file "LICENSE" for information on
usage and redistribution of this file

@[+]cpp_defs.h 0.1.0 
<jack.ogaja+ at +gmail.com> 06-16-14
-----------------------------------------
*/

/*****************

DESCRIPTION:

MACROS for c++ compilers

******************/

#ifdef __cplusplus
#   define BEGIN_DECL_CPP extern "C" {
#   define END_DECL_CPP }
#else
#   define BEGIN_DECL_CPP
#   define END_DECL_CPP
#endif



