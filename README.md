
# HOS tool:
- - - -  

A numerical tool for testing developments of dynamical cores of Geophysical models.  
For example, the results discussed in following publications:  
_"Higher Order Horizontal Discretization of Euler-Equations in
a Non-hydrostatic NWP and RCM Model”_  
- By Jack Ogaja, Cuvillier Verlag; 2016

See [Wiki](https://bitbucket.org/JackOgaja/hos-tool/wiki/Home) for the tool's description.

## The program layout:

This code is modular in design and consist of the following files and folders:  
```
.../hos-tool/  
|- README  
|- LICENSE  
|- CODEOWNERS  
|- Makefie  
|- docs  
|    |- HOSTool.rst 
|    |- TOC.rst
|    |- readme.rst  
|    |- Makefile  
|- bin  
|- include  
|    |- cpp_defs.h  
|    |- hos.h  
|- src  
|    |- hos.c  
|    |- experiments.c  
|    |- ioUtilities.c  
|    |- numericalUtilities.c  
|- experiments  
|    |- conf  
|         |- wave.conf  
|         |- logs  
|              |- run.log  
|         |- output  
|              |- run.out  
|    |- runExperiments.sh  

```

## Releases:

This is an unstable release of the tool! 

[![GitHub (pre-)release](https://img.shields.io/github/release/qubyte/rubidium/all.svg?style=social)](https://github.com/Jaecq/hos-tool/releases)

[//]: # ( -or- )  

[//]: # ( [![GitHub (pre-)release](https://img.shields.io/github/release/qubyte/rubidium/all.svg?style=social)](https://bitbucket.org/JackOgaja/hos-tool/downloads/?tab=tags) )  

## Copyright and License

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](https://github.com/Jaecq/hos-tool/blob/master/LICENSE.md)

HOS Tool is licensed under the MIT license. See [LICENSE](LICENSE.md) file for full copyright information.

## Build:

To build the tool, run the command `make`. Make will create necessary directories if they don't exist.

## Run and experiment:

To run an experiment change to the directory (`cd`) _experiment_ and execute the shell script `runExperiment.sh`


## Tasks:

- [x] hos-tool-serial:  
    * [hos-tool-serial](https://github.com/Jaecq/hos-tool)

- [ ] hos-tool-mpi:   
    * HOS tools with Message Passing Interface Programming Model for parallel execution

- [ ] hos-tool-openmp:  
    * HOS tools with openMP threads for concurrency

- [ ] hos-tool-gpu:  
    * HOS tools with accelator directives for hybrid or GPU computers.



