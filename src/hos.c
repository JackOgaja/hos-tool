/*
-----------------------------------------
Copyright (C) 2014-2016, Jack Ogaja
All rights reserved

See the file "LICENSE" for information on
usage and redistribution of this file

@(+)hos.c
<jack.ogaja@gmail.com> 0.1.0 06-16-14
-----------------------------------------
*/

/*****************

DESCRIPTION:

This is the main program for hosTools:
  1. generates initial and boundary conditions
  2. Calculates fourth order advection

 ******************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef HOS_H
#include "hos.h"
#endif

int
main ()
{
    int i;
    int nx = 50;
    //int ny = 50;
    float *u;
    float dx = 20;
    float dy = 20;

    //float *u_4ord[nx];
    float *u_4ord;
    
    u = malloc( nx * float_bytes); /* define the array for i-b condition */
    if (!u) return NULL;
    //wave(nx,u);  /* generate some intial and boundary condition */
    wave(nx*dx,u);  /* generate some intial and boundary condition */
    
    *u_4ord = fourth_order_qcs(nx,dx,u);
    
    //for (i=0; i<ny; i++)
    for (i=0; i<nx; i++)
    {
        printf("%d: %.6f\n", i, u_4ord[i]);
        printf("%d\n", u);
    }
    
    free(u);
    
    return 0;
}
