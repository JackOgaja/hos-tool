/*
-----------------------------------------
Copyright (C) 2014-2016, Jack Ogaja
All rights reserved

See the file "LICENSE" for information on
usage and redistribution of this file

@(+)experiments.c 
<jack.ogaja@gmail.com> 0.1.0 06-16-14 
-----------------------------------------
*/

/*****************

DESCRIPTION:

This module generates initial and boundary conditions for experiments:
  1. Vectors are defined at the cell surfaces
  2. Scalars are defined at grid centers

Ref: ( with further references therein! )

     1. Ogaja, J., and Will, A., 2016:
     Fourth order conservative discretization
     of horizontal Euler equations in the COSMO
     model and regional climate simulaitons
     - MetZet, DOI 10.1127/metz/2016/0645 

******************/

#include <stdio.h>
#include <math.h>
#include <string.h>
#ifndef HOS_H
#include "hos.h"
#endif

void *
wave ( int nx, float u_wave[])
{
    int i;
    float u[nx];
    float u_t[nx];
    
    srand((unsigned)time(NULL)); /* generate some random number */
    
    for (i = 0; i < nx; i++)
    {
        //u_wave = sin(WAVE_VAR1 * (2 * M_PI) * i / WAVE_VAR2) + sqrt(SQRT_VAR) * rand();
        u[i] = sin(WAVE_VAR1 * (2 * M_PI) * i / WAVE_VAR3) + sqrt(SQRT_VAR) * rand();
        //u_wave = sin(WAVE_VAR1 * (2 * M_PI) * i / WAVE_VAR2) + sqrt(SQRT_VAR);
        
        *u_wave++;
    }
    
    return 0;
}

