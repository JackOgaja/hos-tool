/*
-----------------------------------------
Copyright (C) 2014-2016, Jack Ogaja
All rights reserved

See the file "LICENSE" for information on
usage and redistribution of this file

@(+)numericalUtilities.c 
<jack.ogaja@gmail.com> 0.1.0 06-16-14 
-----------------------------------------
*/

/*****************

DESCRIPTION:

This module defines a regular Arakawa C-grid:
  1. Vectors are defined at the cell surfaces
  2. Scalars are defined at grid centers

Ref: ( with further references therein! )

     1. Ogaja, J., and Will, A., 2016:
     Fourth order conservative discretization
     of horizontal Euler equations in the COSMO
     model and regional climate simulaitons
     - MetZet, DOI 10.1127/metz/2016/0645 

     2. Ogaja, J., 2015: Higher Order Horizontal 
     Discretization of Euler Equations in a 
     Non-Hydrostatic NWP and RCM Model
     - Dissertation, Brandenburgische Technische
       Universitaet Cottbus-Senftenburg
       ( Online: Cuvillier Verlag, 2016,
         ISBN-13: 978-3-73699-294-8/9 ) 

******************/

#include <stdio.h>
#include <string.h>
#ifndef HOS_H
#include "hos.h"
#endif

float *
fourth_order (int nx, float dx, float u[])
{
    int i;
    unsigned int *x;
    unsigned int *y;
    float u_fourth[nx];
    float *t;
    
    /*
    *x = malloc(nx * float_bytes)
    */
    for (i =0; i<=nx; i++)
    {
        u_fourth[i] = (*(u+3) - *(u-3))/dx;
        *u++;
        *u += 1;
    }
    return u_fourth;
}

float *
second_order_qcs (int nx, float dx, float u_s[])
{
    unsigned int istart, iend;
    float u_second[nx];
    
    istart = u_s;
    iend = u_s + nx;
    while ( istart+1 < iend-1 )
    {
        u_second[istart] = (*(u_s+1) - *(u_s-1))/dx;
        ++istart;
        ++u_s;
    }
    return u_second;
}

float *
fourth_order_qcs (int nx, float dx, float u_f[])
{
    unsigned int istart, iend;
    float u_fourth[nx];
    
    istart = u_f;
    iend = u_f + nx;
    //iend = u_f + nx + 1;
    while ( istart+2 < iend-2 )
    {
        u_fourth[istart] = (*(u_f+2) - *(u_f-2))/dx;
        ++istart;
        ++u_f;
    }
    return u_fourth;
}

float *
sixth_order_qcs (int nx, float dx, float u_s[])
{
    unsigned int istart, iend;
    float u_sixth[nx];
    
    istart = u_s;
    iend = u_s + nx;
    while ( istart+3 < iend-3 )
    {
        u_sixth[istart] = (*(u_s+3) - *(u_s-3))/dx;
        ++istart;
        ++u_s;
    }
    return u_sixth;
}






