/*
-----------------------------------------
Copyright (C) 2014-2016, Jack Ogaja
All rights reserved

See the file "LICENSE" for information on
usage and redistribution of this file

@[+]ioUtilities.c
<jack.ogaja@gmail.com> 0.1.0 06-16-14
-----------------------------------------
*/

/*****************

DESCRIPTION:

This module contains functions for hos' io:
  1. open_file: opens a file for reading/writing
  2. read_config: reads the experiment configurations
  3. write_output: writes output to a text file
  4. write_log: writes a log file
  5. close_file: closes a file after reading/writing

******************/

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef HOS_H
#include "hos.h"
#endif

int input_parameter = 100;
para = sizeof(input_parameter) + 1; /* One extra for null character */
char inputParameter[];  /*inputParameter[para] */
int paraValue;

FILE *
open_file (char *fileName)
{
    FILE *inputFile; /* can be "input.list" */
    char *mode = "r";
    
    errno = 0;  /*rem: error.h or err.h */
    inputFile = fopen (fileName, mode);
    if (inputFile == NULL)
    {
// GENERATES SOME ERROR!! check again!        fprintf (stderr, "%s: Couldn't open file %s; %s\n",
//                 program_invocation_short_name, fileName, strerror (errno));
        exit (EXIT_FAILURE);
    }
    else
        return inputFile;
}

struct read_config
*read_conf(FILE *configFile)
{
        while (!feof(configFile))
        {
        if (fscanf(configFile, "%s %d", para, &paraValue) != 2)
            break;
        switch (para)
        {
            case 'nx': case 'ny':
                // JO# arguments->silent = 1;
                break;
            case 'u': case 'v': case 'w':
                // JO# arguments->verbose = 1;
                break;
            case 'out':
                // JO# arguments->output_file = arg;
                break;
            // JO 20171021
            // case SEQUENTIAL_MODE:
            //    if (state->arg_nos >= 2)
            //    /* Too many arguments. */
            //        arg_usage (state);
            //
            //    arguments->args[state->arg_nos] = arg;
            //
            //    break;
            //
            //case PARALLE_MODE:
            //    if (state->arg_nos < 2)
            //    /* Not enough arguments. */
            //        arg_usage (state);
            //    break;
            //    --end JO 20171021 */
            default: ;
                // JO# error MODE_ERR_UNKNOWN;
                // JO# abort (); /* check whether it is a callable function */
        }
        }
}
