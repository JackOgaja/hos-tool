###################################################
#  (C) Copyright 2014-2018 Jack Ogaja             #
#   All rights reserved                           #
#                                                 #
#  This file is part of Hos Tool.                 #
#                                                 #
#  See LICENSE file for information               #
#  regarding permissions to use the tool          #
#  and related files.                             #
###################################################

.SILENT:
.PHONY: all clean clean_all

ROOT   = ${PWD}
SRCDIR = $(ROOT)/src
INCDIR = $(ROOT)/include
OBJDIR = $(ROOT)/tempObj
BINDIR = $(ROOT)/bin
WRKDIR = $(ROOT)/work

PROGRAM = $(BINDIR)/hos.exe

CC = gcc
CPPFLAGS = -Wall #-fpe
INCFLAGS = -I$(INCDIR) 
EXTFLAGS = -fpe
LDFLAGS =
LDLIBS = #-lm

CREATEDIR = mkdir -p
CLEANDIR = rm -rf

SOURCES=$(wildcard $(SRCDIR)/*.c)

OBJFiles = $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SOURCES))

all: $(OBJDIR) $(BINDIR) $(PROGRAM) 

$(PROGRAM): $(OBJFiles) 
	echo ""
	echo "**********"
	echo Creating ...
	( echo $@ | sed 's/.*\(bin\)/\1/g' )
	$(CC) $(INCFLAGS) $(CPPFLAGS) $(CFLAGS) \
	$(LDFLAGS) $(LDLIBS) $^ -o $@
	echo "**********" 
	echo ""

$(OBJDIR)%.o : $(SRCDIR)/%.c
	echo compiling $(@F)...
	$(CC) $(INCFLAGS) $(CPPFLAGS) $(CFLAGS) \
	$(LDFLAGS) $(LDLIBS) -c $< -o $@ 

$(OBJDIR): 
ifneq "$(wildcard $(OBJDIR))" ""
else
	echo
	echo Creating ./tempObj dir...
	$(CREATEDIR) $(OBJDIR)
	echo
endif

$(WRKDIR): 
ifneq "$(wildcard $(WRKDIR))" ""
else
	echo
	echo Creating ./work dir...
	$(CREATEDIR) $(WRKDIR)
	echo
endif

$(BINDIR): 
ifneq "$(wildcard $(BINDIR))" ""
else
	echo
	echo Creating ./bin dir...
	$(CREATEDIR) $(BINDIR)
	echo
endif

clean: ; echo  
	echo Cleaning ./tempObj and ./bin directories...
	$(CLEANDIR) $(PROGRAM) $(OBJFiles) 
	echo

clean_all: ; echo  
	echo removing ./tempObj and ./bin directories...
	$(CLEANDIR) $(BINDIR) $(OBJDIR) 
	echo

#/@END@ mk/#

